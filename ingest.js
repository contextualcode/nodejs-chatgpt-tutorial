import { PineconeClient } from "@pinecone-database/pinecone";
import fs from "fs";
import { OpenAIEmbeddings } from 'langchain/embeddings/openai';
import dotenv from "dotenv";

// Load environment variables from .env file
dotenv.config();

const pineconoe_apiKey = process.env.PINECONE_API_KEY;
const openai_key = process.env.OPENAI_KEY;

const pinecone = new PineconeClient();

const embeddings = new OpenAIEmbeddings({openAIApiKey: openai_key});

async function ingestDocument() {
  try {
    
    await pinecone.init({
      environment: "us-east4-gcp",
      apiKey: pineconoe_apiKey,
    });

    // Read the document content
    const documentContent = fs.readFileSync("/Users/dbroadfoot/nodejs-chatgpt-tutorial/test.md", "utf-8");

    const lines = documentContent.split(/\r?\n/);

    // Process lines to create sections
    const sections = [];
    let currentSection = null;
    
    for (var line of lines) {
      line = line.trimStart();

      if (line.match(/^#+/)) {
        // Found a heading
        if (currentSection) {
          sections.push(currentSection);
        }
        currentSection = {
          heading: line.trim(),
          content: []
        };
      } else if (currentSection) {
        // Add content to the current section
        currentSection.content.push(line);
      }
    }
    
    // Push the last section if there's one remaining
    if (currentSection) {
      sections.push(currentSection);
    }
    
    // Convert sections to objects with text and metadata
    const parsedSections = sections.map(section => {
      return {
        text: section.content.join("\n"),
        metadata: {
          heading: section.heading
        }
      };
    });

    const tokenizedData = tokenizeDocument(documentContent);

    async function getEmbedding(token) {
        try {
          const embedding = await embeddings.embedDocuments(token); // 'en' for English
          return embedding;
        } catch (error) {
          console.error(`Error retrieving embedding for document:`, error.message);
          return null;
        }
      }

    // Prepare data for ingestion
    const promises = tokenizedData.map(async (sentence, idx) => {
        const sentenceVector = await getEmbedding(sentence); // Wait for the promise to resolve
        const vectorObjects = sentenceVector.map((flattenedVector, vectorIdx) => {
            return {
              id: `${idx}_${vectorIdx}`,
              values: flattenedVector, // Use the resolved vector
              metadata: {
                section: "example-section",
              },
            };
          });
          return vectorObjects;
      });

    const dataToIngest = await Promise.all(promises);

    const mergedDataToIngest = [].concat(...dataToIngest);

    const indexName = "cc-test";
    const namespace = "broadfoot-cars";

    const index = pinecone.Index(indexName);
    
    const upsertRequest = {
        upsertRequest: {
            vectors: mergedDataToIngest,
            namespace: namespace
        }
    };

    const upsertResponse = await index.upsert(upsertRequest);

    console.log("Data ingested successfully:", upsertResponse);
  } catch (error) {
    console.error("Error ingesting data:", error.message);
  }
}

function tokenizeDocument(content) {
  // A simple example of tokenization: Split content by whitespace
  const sentences = content.split(/\n+/);
  const tokenizedData = sentences.map(sentence => sentence.split(/\s+/));

  return tokenizedData;
}

// Call the ingestDocument function to start the ingestion process
ingestDocument();
